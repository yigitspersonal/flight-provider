package com.tokigames.challenge.flightprovider.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tokigames.challenge.flightprovider.controller.FlightController.ErrorResponse;
import com.tokigames.challenge.flightprovider.model.FilterBy;
import com.tokigames.challenge.flightprovider.model.FilterDefinition;
import com.tokigames.challenge.flightprovider.model.FlightType;
import com.tokigames.challenge.flightprovider.model.PageDefinition;
import com.tokigames.challenge.flightprovider.model.SortBy;
import com.tokigames.challenge.flightprovider.model.SortDefinition;
import com.tokigames.challenge.flightprovider.service.FlightService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(FlightController.class)
public class FlightControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private FlightService flightService;

  @Captor
  private ArgumentCaptor<Collection<FilterDefinition>> flightFiltersCaptor;

  @Captor
  private ArgumentCaptor<SortDefinition> sortDefinitionCaptor;

  @Captor
  private ArgumentCaptor<PageDefinition>  pageDefinitionCaptor;

  private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  @Before
  public void initMocks() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getFlightsNoFilter() throws Exception {
    // given, when
    mockMvc.perform(get("/flights")).andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    assertTrue(flightFiltersCaptor.getValue().isEmpty());
  }

  @Test
  public void getFlightsTypeFilter() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("flight_type", "CHEAP"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.TYPE, passedDefinition.getFilterBy());
    assertEquals(FlightType.CHEAP, passedDefinition.getFilterValue());
  }

  @Test
  public void getFlightsDepartureFilter() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("departure", "Atlantis"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.DEPARTURE_CITY, passedDefinition.getFilterBy());
    assertEquals("Atlantis", passedDefinition.getFilterValue());

  }

  @Test
  public void getFlightsArrivalFilter() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("arrival", "Mordor"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.ARRIVAL_CITY, passedDefinition.getFilterBy());
    assertEquals("Mordor", passedDefinition.getFilterValue());

  }

  @Test
  public void getFlightsCityFilter() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("city", "Minoa"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.CITY, passedDefinition.getFilterBy());
    assertEquals("Minoa", passedDefinition.getFilterValue());

  }

  @Test
  public void getFlightsDepartureAfterFilter() throws Exception {
    // given, when
    String filterDate = "2019-05-19 03:00";
    mockMvc.perform(get("/flights").param("departure_after", filterDate))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.DEPARTURE_TIME_EARLIEST, passedDefinition.getFilterBy());
    assertEquals(filterDate, df.format((Date) passedDefinition.getFilterValue()));

  }

  @Test
  public void getFlightsDepartureBeforeFilter() throws Exception {
    // given, when
    String filterDate = "2019-05-19 03:00";
    mockMvc.perform(get("/flights").param("departure_before", filterDate))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.DEPARTURE_TIME_LATEST, passedDefinition.getFilterBy());
    assertEquals(filterDate, df.format((Date) passedDefinition.getFilterValue()));

  }

  @Test
  public void getFlightsArrivalAfterFilter() throws Exception {
    // given, when
    String filterDate = "2019-05-19 03:00";
    mockMvc.perform(get("/flights").param("arrival_after", filterDate))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.ARRIVAL_TIME_EARLIEST, passedDefinition.getFilterBy());
    assertEquals(filterDate, df.format((Date) passedDefinition.getFilterValue()));

  }

  @Test
  public void getFlightsArrivalBeforeFilter() throws Exception {
    // given, when
    String filterDate = "2019-05-19 03:00";
    mockMvc.perform(get("/flights").param("arrival_before", filterDate))
        .andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(1, passedFilters.size());
    FilterDefinition passedDefinition = passedFilters.iterator().next();
    assertEquals(FilterBy.ARRIVAL_TIME_LATEST, passedDefinition.getFilterBy());
    assertEquals(filterDate, df.format((Date) passedDefinition.getFilterValue()));

  }

  @Test
  public void getFlightsDepartureBetweenFilters() throws Exception {
    // given
    String filterDateAfter = "2019-05-19 03:00";
    String filterDateBefore = "2020-06-20 04:00";

    // when
    mockMvc.perform(get("/flights").param("departure_after", filterDateAfter)
        .param("departure_before", filterDateBefore)).andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(2, passedFilters.size());
    assertTrue(passedFilters.stream().anyMatch(f ->
        FilterBy.DEPARTURE_TIME_EARLIEST.equals(f.getFilterBy())
            && filterDateAfter.equals(df.format((Date) f.getFilterValue()))
    ));
    assertTrue(passedFilters.stream().anyMatch(f ->
        FilterBy.DEPARTURE_TIME_LATEST.equals(f.getFilterBy())
            && filterDateBefore.equals(df.format((Date) f.getFilterValue()))
    ));

  }

  @Test
  public void getFlightsTypeAndDepartureAndArrivalFilters() throws Exception {
    // given
    String departure = "Shire";
    String arrival = "Mordor";

    // when
    mockMvc.perform(get("/flights").param("flight_type", "BUSINESS")
        .param("departure", departure).param("arrival", arrival)).andExpect(status().isOk());
    verify(flightService).getFlights(flightFiltersCaptor.capture(), any(SortDefinition.class),
        any(PageDefinition.class));

    // then
    Collection<FilterDefinition> passedFilters = flightFiltersCaptor.getValue();
    assertEquals(3, passedFilters.size());
    assertTrue(passedFilters.stream().anyMatch(f ->
        FilterBy.DEPARTURE_CITY.equals(f.getFilterBy()) && departure.equals(f.getFilterValue()))
    );
    assertTrue(passedFilters.stream().anyMatch(f ->
        FilterBy.ARRIVAL_CITY.equals(f.getFilterBy()) && arrival.equals(f.getFilterValue()))
    );
    assertTrue(passedFilters.stream().anyMatch(f ->
        FilterBy.TYPE.equals(f.getFilterBy()) && FlightType.BUSINESS.equals(f.getFilterValue()))
    );

  }

  @Test
  public void getFlightsSortedByType() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "TYPE"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.TYPE, sortDefinitionCaptor.getValue().getSortBy());
    assertFalse(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByDepartureCity() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "DEPARTURE_CITY"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.DEPARTURE_CITY, sortDefinitionCaptor.getValue().getSortBy());
    assertFalse(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByArrivalCity() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "ARRIVAL_CITY"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.ARRIVAL_CITY, sortDefinitionCaptor.getValue().getSortBy());
    assertFalse(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByDepartureTime() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "DEPARTURE_TIME"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.DEPARTURE_TIME, sortDefinitionCaptor.getValue().getSortBy());
    assertFalse(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByArrivalTime() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "ARRIVAL_TIME"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.ARRIVAL_TIME, sortDefinitionCaptor.getValue().getSortBy());
    assertFalse(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByTypeDescending() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "TYPE")
        .param("desc", "true")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.TYPE, sortDefinitionCaptor.getValue().getSortBy());
    assertTrue(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByDepartureCityDescending() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "DEPARTURE_CITY")
        .param("desc", "true")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.DEPARTURE_CITY, sortDefinitionCaptor.getValue().getSortBy());
    assertTrue(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByArrivalCityDescending() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "ARRIVAL_CITY")
        .param("desc", "true")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.ARRIVAL_CITY, sortDefinitionCaptor.getValue().getSortBy());
    assertTrue(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByDepartureTimeDescending() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "DEPARTURE_TIME")
        .param("desc", "true")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.DEPARTURE_TIME, sortDefinitionCaptor.getValue().getSortBy());
    assertTrue(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void getFlightsSortedByArrivalTimeDescending() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("sort_by", "ARRIVAL_TIME")
        .param("desc", "true")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), sortDefinitionCaptor.capture(),
        any(PageDefinition.class));

    // then
    assertEquals(SortBy.ARRIVAL_TIME, sortDefinitionCaptor.getValue().getSortBy());
    assertTrue(sortDefinitionCaptor.getValue().getDescending());
  }

  @Test
  public void noLimitAndPage_pagesFirst10() throws Exception {
    // given, when
    mockMvc.perform(get("/flights")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), any(SortDefinition.class),
        pageDefinitionCaptor.capture());

    // then
    PageDefinition captured = pageDefinitionCaptor.getValue();
    assertEquals(0, captured.getOffset().intValue());
    assertEquals(10, captured.getLimit().intValue());
  }

  @Test
  public void limit20AndPage2_pagesBetween20And40() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("limit", "20")
        .param("page", "2")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), any(SortDefinition.class),
        pageDefinitionCaptor.capture());

    // then
    PageDefinition captured = pageDefinitionCaptor.getValue();
    assertEquals(20, captured.getOffset().intValue());
    assertEquals(20, captured.getLimit().intValue());
  }

  @Test
  public void limit80_pagesFirst50() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("limit", "80"))
        .andExpect(status().isOk());
    verify(flightService).getFlights(any(), any(SortDefinition.class),
        pageDefinitionCaptor.capture());

    // then
    PageDefinition captured = pageDefinitionCaptor.getValue();
    assertEquals(0, captured.getOffset().intValue());
    assertEquals(50, captured.getLimit().intValue());
  }

  @Test
  public void limit16AndPage3_pagesBetween20And30() throws Exception {
    // given, when
    mockMvc.perform(get("/flights").param("limit", "16")
        .param("page", "3")).andExpect(status().isOk());
    verify(flightService).getFlights(any(), any(SortDefinition.class),
        pageDefinitionCaptor.capture());

    // then
    PageDefinition captured = pageDefinitionCaptor.getValue();
    assertEquals(20, captured.getOffset().intValue());
    assertEquals(10, captured.getLimit().intValue());
  }

  @Test
  public void limitSmallerThan10_throwsException() throws Exception {
    // given, when
    MvcResult result = mockMvc.perform(get("/flights").param("limit", "9"))
        .andExpect(status().isOk()).andReturn();

    // then
    ObjectMapper mapper = new ObjectMapper();
    ErrorResponse errorResponse = mapper.readValue(result.getResponse().getContentAsString(),
        ErrorResponse.class);
    assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatus());
    assertEquals("Validation error", errorResponse.getError());
  }

  @Test
  public void pageNonPositive_throwsException() throws Exception {
    // given, when
    MvcResult result = mockMvc.perform(get("/flights").param("page", "0"))
        .andExpect(status().isOk()).andReturn();

    // then
    ObjectMapper mapper = new ObjectMapper();
    ErrorResponse errorResponse = mapper.readValue(result.getResponse().getContentAsString(),
        ErrorResponse.class);
    assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatus());
    assertEquals("Validation error", errorResponse.getError());
  }
}
