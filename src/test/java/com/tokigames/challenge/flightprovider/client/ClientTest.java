package com.tokigames.challenge.flightprovider.client;

import static org.junit.Assert.assertFalse;

import com.tokigames.challenge.flightprovider.model.BusinessFlight;
import com.tokigames.challenge.flightprovider.model.CheapFlight;
import java.util.Collection;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Ignore these tests if you do not want to wait, but always keep them in main branches.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class ClientTest {

  @Autowired
  private BusinessFlightClient businessClient;

  @Autowired
  private CheapFlightClient cheapClient;

  @Test
  public void cheapClientGetsList() {
    Collection<CheapFlight> flights = cheapClient.getAllFlightsAsync().collect(Collectors.toList())
        .block();
    assertFalse(flights.isEmpty());
  }

  @Test
  public void businessClientGetsList() {
    Collection<BusinessFlight> flights = businessClient.getAllFlightsAsync().collect(
        Collectors.toList()).block();
    assertFalse(flights.isEmpty());
  }
}
