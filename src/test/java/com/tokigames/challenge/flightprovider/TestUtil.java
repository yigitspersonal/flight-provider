package com.tokigames.challenge.flightprovider;

import com.tokigames.challenge.flightprovider.model.BusinessFlight;
import com.tokigames.challenge.flightprovider.model.CheapFlight;
import com.tokigames.challenge.flightprovider.model.Flight;
import com.tokigames.challenge.flightprovider.model.FlightType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestUtil {

  private static final long startTime = 1262296800000L;

  private static final long endTime = new Date().getTime();

  private static final Random random = new Random();

  private static final String[] cities = {"Istanbul", "Antalya", "Ankara", "London", "Berlin",
      "New York", "Los Angeles", "Boston", "Chicago", "München", "Moskva", "St.Peterburg",
      "Kharkiv", "Kiev", "Minsk", "Rome", "Vienna", "Paris", "Milano", "Venice", "Warsaw",
      "Amsterdam", "Brussels", "Stockholm", "Oslo", "Helsinki", "Zürich", "Geneva", "Madrid",
      "Barcelona", "Lisbon", "Prag", "Bratislava", "Sofia", "Athens", "Sarajevo", "Beijing",
      "Tokyo", "Hong Kong", "Singapore", "Melbourne", "Sydney", "Bangkok", "Cairo", "Kuala Lumpur",
      "Johannesburg", "Buenos Aires", "Rio de Janeiro", "Nairobi", "Marrakesh", "Alexandria"
  };

  public static Collection<BusinessFlight> getSampleBusinessFlights() {
    return Arrays.asList(
        BusinessFlight.builder()
            .departure("Istanbul").arrival("Antalya")
            .departureTime(new Date(1558902656000L)).arrivalTime(new Date(1558902956000L)).build(),
        BusinessFlight.builder()
            .departure("Singapore").arrival("Amsterdam")
            .departureTime(new Date(1558935605000L)).arrivalTime(new Date(1558935905000L))
            .build(),
        BusinessFlight.builder()
            .departure("London").arrival("Moscow")
            .departureTime(new Date(1563588000000L)).arrivalTime(new Date(1563678000000L))
            .build()
    );
  }

  public static Collection<CheapFlight> getSampleCheapFlights() {
    return Arrays.asList(
        CheapFlight.builder()
            .route("Cruz del Eje-Tizi")
            .departure(new Date(1558902657000L)).arrival(new Date(1558902756000L))
            .build(),
        CheapFlight.builder()
            .route("Boston-Tokyo")
            .departure(new Date(1561627856000L)).arrival(new Date(1564410656000L))
            .build()
    );
  }

  public static Collection<Flight> getSampleFlights() {
    return Arrays.asList(
        Flight.builder().departure("Istanbul").arrival("Antalya").type(FlightType.BUSINESS)
            .departureTime(new Date(1558902656000L)).arrivalTime(new Date(1558902956000L)).build(),
        Flight.builder().departure("Singapore").arrival("Amsterdam").type(FlightType.BUSINESS)
            .departureTime(new Date(1558935605000L)).arrivalTime(new Date(1558935905000L)).build(),
        Flight.builder().departure("London").arrival("Moscow").type(FlightType.BUSINESS)
            .departureTime(new Date(1563588000000L)).arrivalTime(new Date(1563678000000L)).build(),
        Flight.builder().departure("Cruz del Eje").arrival("Tizi").type(FlightType.CHEAP)
            .departureTime(new Date(1558902657000L)).arrivalTime(new Date(1558902756000L)).build(),
        Flight.builder().departure("Boston").arrival("Tokyo").type(FlightType.CHEAP)
            .departureTime(new Date(1561627856000L)).arrivalTime(new Date(1564410656000L)).build()
    );
  }

  public static Stream<Flight> getUnfilteredFlightStream() throws ParseException {
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    return Stream.of(
        Flight.builder().departure("Istanbul").arrival("Antalya").type(FlightType.CHEAP)
            .departureTime(df.parse("19-05-2019 13:00"))
            .arrivalTime(df.parse("19-05-2019 14:10")).build(),
        Flight.builder().departure("Istanbul").arrival("Amsterdam").type(FlightType.BUSINESS)
            .departureTime(df.parse("18-05-2019 12:00"))
            .arrivalTime(df.parse("18-05-2019 15:10")).build(),
        Flight.builder().departure("Singapore").arrival("Istanbul").type(FlightType.CHEAP)
            .departureTime(df.parse("17-05-2019 03:00"))
            .arrivalTime(df.parse("17-05-2019 11:20")).build(),
        Flight.builder().departure("Moscow").arrival("Singapore").type(FlightType.BUSINESS)
            .departureTime(df.parse("12-04-2019 14:00"))
            .arrivalTime(df.parse("13-04-2019 01:00")).build(),
        Flight.builder().departure("Boston").arrival("Amsterdam").type(FlightType.CHEAP)
            .departureTime(df.parse("19-05-2019 03:00"))
            .arrivalTime(df.parse("19-05-2019 15:10")).build(),
        Flight.builder().departure("Rio de Janeiro").arrival("London").type(FlightType.BUSINESS)
            .departureTime(df.parse("20-07-2019 03:00"))
            .arrivalTime(df.parse("20-07-2019 04:10")).build(),
        Flight.builder().departure("Tokyo").arrival("Berlin").type(FlightType.BUSINESS)
            .departureTime(df.parse("19-07-2019 03:00"))
            .arrivalTime(df.parse("15-07-2019 04:10")).build(),
        Flight.builder().departure("Tokyo").arrival("Berlin").type(FlightType.BUSINESS)
            .departureTime(df.parse("19-06-2019 03:00"))
            .arrivalTime(df.parse("15-06-2019 04:10")).build()
    );
  }

  public static Stream<Flight> getRandomFlights(int count) {
    return IntStream.range(0, count).mapToObj(i -> getRandomFlight());
  }

  private static Flight getRandomFlight() {
    Date t1 = getRandomDate();
    Date t2 = getRandomDate();
    return Flight.builder().departure(getRandomCity()).arrival(getRandomCity())
        .departureTime(t1.before(t2) ? t1 : t2).arrivalTime(t1.before(t2) ? t2 : t1)
        .type(random.nextInt(100) < 50 ? FlightType.BUSINESS : FlightType.CHEAP)
        .build();
  }

  private static Date getRandomDate() {
    long time = random.longs(startTime, endTime).findAny().orElse(startTime);
    return new Date(time);
  }

  private static String getRandomCity() {
    return cities[random.nextInt(cities.length)];
  }
}
