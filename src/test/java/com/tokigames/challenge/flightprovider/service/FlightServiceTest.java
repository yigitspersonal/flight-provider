package com.tokigames.challenge.flightprovider.service;

import static com.tokigames.challenge.flightprovider.model.PageDefinition.DEFAULT_PAGE;
import static com.tokigames.challenge.flightprovider.model.SortDefinition.UNSORTED;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.tokigames.challenge.flightprovider.TestUtil;
import com.tokigames.challenge.flightprovider.client.BusinessFlightClient;
import com.tokigames.challenge.flightprovider.client.CheapFlightClient;
import com.tokigames.challenge.flightprovider.model.BusinessFlight;
import com.tokigames.challenge.flightprovider.model.CheapFlight;
import com.tokigames.challenge.flightprovider.model.FilterBy;
import com.tokigames.challenge.flightprovider.model.FilterDefinition;
import com.tokigames.challenge.flightprovider.model.Flight;
import com.tokigames.challenge.flightprovider.model.FlightType;
import com.tokigames.challenge.flightprovider.model.PageDefinition;
import com.tokigames.challenge.flightprovider.model.SortBy;
import com.tokigames.challenge.flightprovider.model.SortDefinition;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class FlightServiceTest {

  @Autowired
  private FlightService flightService;

  @MockBean
  private BusinessFlightClient businessFlightClient;

  @MockBean
  private CheapFlightClient cheapFlightClient;

  private FlightService mockedService;

  @Before
  public void setUpMock() throws ParseException {
    mockedService = spy(flightService);
    doReturn(TestUtil.getUnfilteredFlightStream()).when(mockedService).getAllFlightStream();
  }

  @Test
  public void getAllFlightsCollatesBoth() {
    // given
    Flux<BusinessFlight> businessFlights = Flux.fromIterable(TestUtil.getSampleBusinessFlights());
    Flux<CheapFlight> cheapFlights = Flux.fromIterable(TestUtil.getSampleCheapFlights());
    when(businessFlightClient.getAllFlightsAsync()).thenReturn(businessFlights);
    when(cheapFlightClient.getAllFlightsAsync()).thenReturn(cheapFlights);

    // when
    Collection<Flight> result = flightService.getAllFlights();

    // then
    assertFalse(result.isEmpty());
    Collection<Flight> expected = TestUtil.getSampleFlights();
    assertArrayEquals(expected.toArray(), result.toArray());
  }

  /*
   * by cheap returns istanbul->antalya, singapore->istanbul, boston->amsterdam
   */
  @Test
  public void getFlightsFilteredByType() {
    // given
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.TYPE, FlightType.CHEAP));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(3, filtered.size());
    assertTrue(filtered.stream().anyMatch(f ->
        "Istanbul".equals(f.getDeparture()) && "Antalya".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Singapore".equals(f.getDeparture()) && "Istanbul".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Boston".equals(f.getDeparture()) && "Amsterdam".equals(f.getArrival())));

  }

  /*
   * by Istanbul returns ->Antalya, ->Amsterdam
   */
  @Test
  public void getFlightsFilteredByDepartureCity() {
    // given
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_CITY, "Istanbul"));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(2, filtered.size());
    assertTrue(filtered.stream().anyMatch(f -> "Antalya".equals(f.getArrival())));
    assertTrue(filtered.stream().anyMatch(f -> "Amsterdam".equals(f.getArrival())));
  }

  /*
   * by Amsterdam returns Istanbul->, Boston->
   */
  @Test
  public void getFlightsFilteredByArrivalCity() {
    // given
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.ARRIVAL_CITY, "Amsterdam"));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(2, filtered.size());
    assertTrue(filtered.stream().anyMatch(f -> "Istanbul".equals(f.getDeparture())));
    assertTrue(filtered.stream().anyMatch(f -> "Boston".equals(f.getDeparture())));
  }

  /*
   * by istanbul returns istanbul->antalya, istanbul->amsterdam, singapore->istanbul
   */
  @Test
  public void getFlightsFilteredByCity() {
    // given
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.CITY, "Istanbul"));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(3, filtered.size());
    assertTrue(filtered.stream().anyMatch(f ->
        "Istanbul".equals(f.getDeparture()) && "Antalya".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Istanbul".equals(f.getDeparture()) && "Amsterdam".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Singapore".equals(f.getDeparture()) && "Istanbul".equals(f.getArrival())));
  }

  /*
   * by 19-07-2019,12:00 returns rio->london
   */
  @Test
  public void getFlightsFilteredByDepartureTimeMin() throws ParseException {
    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date after = df.parse("19-07-2019 12:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_EARLIEST, after));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(1, filtered.size());
    Flight result = filtered.iterator().next();
    assertEquals("Rio de Janeiro", result.getDeparture());
    assertEquals("London", result.getArrival());
  }

  /*
   * by 13-04-2019 returns moscow->singapore
   */
  @Test
  public void getFlightsFilteredByDepartureTimeMax() throws ParseException {
    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date before = df.parse("13-04-2019 00:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_LATEST, before));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(1, filtered.size());
    Flight result = filtered.iterator().next();
    assertEquals("Moscow", result.getDeparture());
    assertEquals("Singapore", result.getArrival());
  }

  /*
   * by 18-07-2019 returns rio->london
   */
  @Test
  public void getFlightsFilteredByArrivalTimeMin() throws ParseException {
    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date after = df.parse("18-07-2019 00:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.ARRIVAL_TIME_EARLIEST, after));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(1, filtered.size());
    Flight result = filtered.iterator().next();
    assertEquals("Rio de Janeiro", result.getDeparture());
    assertEquals("London", result.getArrival());
  }

  /*
   * by 14-04-2019 returns moscow->singapore
   */
  @Test
  public void getFlightsFilteredByArrivalTimeMax() throws ParseException {
    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date before = df.parse("14-04-2019 00:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.ARRIVAL_TIME_LATEST, before));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(1, filtered.size());
    Flight result = filtered.iterator().next();
    assertEquals("Moscow", result.getDeparture());
    assertEquals("Singapore", result.getArrival());
  }

  /*
   * between 18-05-2019,00:00 and 19-05-2019,12:00 returns istanbul->amsterdam, boston->amsterdam
   */
  @Test
  public void getFlightsFilteredByDepartureTimeBetween() throws ParseException {
    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date after = df.parse("18-05-2019 00:00");
    Date before = df.parse("19-05-2019 12:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_EARLIEST, after));
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_LATEST, before));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(2, filtered.size());
    assertTrue(filtered.stream().anyMatch(f ->
        "Boston".equals(f.getDeparture()) && "Amsterdam".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Istanbul".equals(f.getDeparture()) && "Amsterdam".equals(f.getArrival())));
  }

  /*
   * by cheap and istanbul returns istanbul->antalya, singapore->istanbul
   */
  @Test
  public void getFlightsFilteredByTypeAndCity() {
    // given
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.TYPE, FlightType.CHEAP));
    filters.add(new FilterDefinition(FilterBy.CITY, "Istanbul"));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(2, filtered.size());
    assertTrue(filtered.stream().anyMatch(f ->
        "Istanbul".equals(f.getDeparture()) && "Antalya".equals(f.getArrival())));

    assertTrue(filtered.stream().anyMatch(f ->
        "Singapore".equals(f.getDeparture()) && "Istanbul".equals(f.getArrival())));
  }

  // by Tokyo to Berlin And 20-06-2019 returns one
  @Test
  public void getFlightsFilteredByDepartureCityAndArrivalCityAndDepartureTimeMax()
      throws ParseException {

    // given
    DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    Date before = df.parse("20-06-2019 00:00");
    ArrayList<FilterDefinition> filters = new ArrayList<>();
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_CITY, "Tokyo"));
    filters.add(new FilterDefinition(FilterBy.ARRIVAL_CITY, "Berlin"));
    filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_LATEST, before));

    // when
    Collection<Flight> filtered = mockedService.getFlights(filters, UNSORTED, DEFAULT_PAGE);

    // then
    assertEquals(1, filtered.size());
    Flight result = filtered.iterator().next();
    assertEquals("Tokyo", result.getDeparture());
    assertEquals("Berlin", result.getArrival());
    assertTrue(before.after(result.getDepartureTime()));

  }

  @Test
  public void sortFlightsByType() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.TYPE, false);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertTrue(orderedFlights.subList(0, 5).stream().allMatch(f ->
        FlightType.BUSINESS.equals(f.getType())));
    assertTrue(orderedFlights.subList(5, orderedFlights.size()).stream().allMatch(f ->
        FlightType.CHEAP.equals(f.getType())));
  }

  @Test
  public void sortFlightsByDepartureCity() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.DEPARTURE_CITY, false);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Boston", orderedFlights.get(0).getDeparture());
    assertEquals("Tokyo", orderedFlights.get(orderedFlights.size() - 1).getDeparture());
  }

  @Test
  public void sortFlightsByArrivalCity() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.ARRIVAL_CITY, false);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Amsterdam", orderedFlights.get(0).getArrival());
    assertEquals("Singapore", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void sortFlightsByDepartureTime() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.DEPARTURE_TIME, false);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Moscow", orderedFlights.get(0).getDeparture());
    assertEquals("Singapore", orderedFlights.get(0).getArrival());
    assertEquals("Rio de Janeiro", orderedFlights.get(orderedFlights.size() - 1)
        .getDeparture());
    assertEquals("London", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void sortFlightsByArrivalTime() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.ARRIVAL_TIME, false);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Moscow", orderedFlights.get(0).getDeparture());
    assertEquals("Singapore", orderedFlights.get(0).getArrival());
    assertEquals("Rio de Janeiro", orderedFlights.get(orderedFlights.size() - 1)
        .getDeparture());
    assertEquals("London", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void sortFlightsByTypeDescending() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.TYPE, true);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertTrue(orderedFlights.subList(0, 3).stream().allMatch(f ->
        FlightType.CHEAP.equals(f.getType())));
    assertTrue(orderedFlights.subList(3, orderedFlights.size()).stream().allMatch(f ->
        FlightType.BUSINESS.equals(f.getType())));
  }

  @Test
  public void sortFlightsByDepartureCityDescending() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.DEPARTURE_CITY, true);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Tokyo", orderedFlights.get(0).getDeparture());
    assertEquals("Boston", orderedFlights.get(orderedFlights.size() - 1).getDeparture());
  }

  @Test
  public void sortFlightsByArrivalCityDescending() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.ARRIVAL_CITY, true);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Singapore", orderedFlights.get(0).getArrival());
    assertEquals("Amsterdam", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void sortFlightsByDepartureTimeDescending() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.DEPARTURE_TIME, true);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Rio de Janeiro", orderedFlights.get(0).getDeparture());
    assertEquals("London", orderedFlights.get(0).getArrival());
    assertEquals("Moscow", orderedFlights.get(orderedFlights.size() - 1)
        .getDeparture());
    assertEquals("Singapore", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void sortFlightsByArrivalTimeDescending() throws ParseException {
    // given
    Stream<Flight> unorderedFlightStream = TestUtil.getUnfilteredFlightStream();
    SortDefinition sortDefinition = new SortDefinition(SortBy.ARRIVAL_TIME, true);

    // when
    List<Flight> orderedFlights = flightService.sortFlightsBy(unorderedFlightStream, sortDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals("Rio de Janeiro", orderedFlights.get(0).getDeparture());
    assertEquals("London", orderedFlights.get(0).getArrival());
    assertEquals("Moscow", orderedFlights.get(orderedFlights.size() - 1)
        .getDeparture());
    assertEquals("Singapore", orderedFlights.get(orderedFlights.size() - 1).getArrival());
  }

  @Test
  public void limit10AndPage1_returnsFirst10() {
    // given
    List<Flight> flightList = TestUtil.getRandomFlights(20)
        .collect(Collectors.toList());
    assertEquals(20, flightList.size()); // implicit testing of the TestUtil
    PageDefinition pageDefinition = new PageDefinition(10, 1);

    // when
    List<Flight> paged = flightService.pageFlightsBy(flightList.stream(), pageDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals(10, paged.size());
    Iterator<Flight> expectedFlights = flightList.subList(0, 10).iterator();
    assertTrue(paged.stream().allMatch(f -> f.equals(expectedFlights.next())));
  }

  @Test
  public void limit20AndPage3_returnsBetween40And60() {
    // given
    List<Flight> flightList = TestUtil.getRandomFlights(80)
        .collect(Collectors.toList());
    PageDefinition pageDefinition = new PageDefinition(20, 3);

    // when
    List<Flight> paged = flightService.pageFlightsBy(flightList.stream(), pageDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals(20, paged.size());
    Iterator<Flight> expectedFlights = flightList.subList(40, 60).iterator();
    assertTrue(paged.stream().allMatch(f -> f.equals(expectedFlights.next())));
  }

  @Test
  public void page1_returnsAllWhenLessThanLimit() {
    // given
    List<Flight> flightList = TestUtil.getRandomFlights(8)
        .collect(Collectors.toList());
    PageDefinition pageDefinition = new PageDefinition(20, 1);

    // when
    List<Flight> paged = flightService.pageFlightsBy(flightList.stream(), pageDefinition)
        .collect(Collectors.toList());

    // then
    assertEquals(8, paged.size());
    Iterator<Flight> expectedFlights = flightList.iterator();
    assertTrue(paged.stream().allMatch(f -> f.equals(expectedFlights.next())));
  }

  @Test
  public void page_returnsEmptyWhenOutOfBounds() {
    // given
    List<Flight> flightList = TestUtil.getRandomFlights(22)
        .collect(Collectors.toList());
    PageDefinition pageDefinition = new PageDefinition(20, 3);

    // when
    Stream<Flight> paged = flightService.pageFlightsBy(flightList.stream(), pageDefinition);

    // then
    assertEquals(0, paged.count());
  }
}
