package com.tokigames.challenge.flightprovider.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tokigames.challenge.flightprovider.util.FlightDateDeserializer;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheapFlight {

  private String route;

  @JsonDeserialize(converter = FlightDateDeserializer.class)
  private Date departure;

  @JsonDeserialize(converter = FlightDateDeserializer.class)
  private Date arrival;

  public String getDepartureCity() {
    return getRouteToken(0);
  }

  public String getArrivalCity() {
    return getRouteToken(1);
  }

  /*
   * Splits the route string into two and returns either the departure (1st) or the arrival (2nd)
   * token.
   */
  private String getRouteToken(int index) {
    if (route == null) {
      return null;
    }
    String[] tokens = route.split("-");
    return tokens.length == 2 ? tokens[index] : "";
  }
}
