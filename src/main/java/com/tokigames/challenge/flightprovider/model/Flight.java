package com.tokigames.challenge.flightprovider.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

  private String departure;

  private String arrival;

  @JsonFormat(pattern = "dd-MMM-yyyy hh:mm")
  private Date departureTime;

  @JsonFormat(pattern = "dd-MMM-yyyy hh:mm")
  private Date arrivalTime;

  private FlightType type;

}
