package com.tokigames.challenge.flightprovider.model;

import java.util.Collection;
import lombok.Data;

@Data
public class FlightListContainer<T> {

  private Collection<T> data;

}
