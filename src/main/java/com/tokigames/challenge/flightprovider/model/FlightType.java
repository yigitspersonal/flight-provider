package com.tokigames.challenge.flightprovider.model;

public enum FlightType {
  BUSINESS, CHEAP
}