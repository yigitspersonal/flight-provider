package com.tokigames.challenge.flightprovider.model;

import lombok.Getter;

@Getter
public class PageDefinition {

  public static final PageDefinition DEFAULT_PAGE = new PageDefinition(10, 1);

  private Integer limit;

  private Integer page;

  public PageDefinition(Integer limit, Integer page) {
    // Make sure the page size is divisible by 10
    this.limit = limit - (limit % 10);
    if (this.limit > 50) {
      this.limit = 50;
    }
    this.page = page;
  }

  public Integer getOffset() {
    return (page - 1) * limit;
  }
}
