package com.tokigames.challenge.flightprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SortDefinition {

  public static final SortDefinition UNSORTED = new SortDefinition(SortBy.NONE, false);

  private SortBy sortBy;

  private Boolean descending;

}
