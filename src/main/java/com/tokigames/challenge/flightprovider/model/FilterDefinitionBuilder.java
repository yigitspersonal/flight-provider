package com.tokigames.challenge.flightprovider.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

public class FilterDefinitionBuilder {

  private FlightType flightType;

  private String departureCity;

  private String arrivalCity;

  private String city;

  private Date departureEarliest;

  private Date departureLatest;

  private Date arrivalEarliest;

  private Date arrivalLatest;

  public static FilterDefinitionBuilder builder() {
    return new FilterDefinitionBuilder();
  }

  private FilterDefinitionBuilder() { }

  public Collection<FilterDefinition> build() {
    final ArrayList<FilterDefinition> filters = new ArrayList<>();
    if (Objects.nonNull(flightType)) {
      filters.add(new FilterDefinition(FilterBy.TYPE, flightType));
    }
    if (Objects.nonNull(departureCity) && !departureCity.isEmpty()) {
      filters.add(new FilterDefinition(FilterBy.DEPARTURE_CITY, departureCity));
    }
    if (Objects.nonNull(arrivalCity) && !arrivalCity.isEmpty()) {
      filters.add(new FilterDefinition(FilterBy.ARRIVAL_CITY, arrivalCity));
    }
    if (Objects.nonNull(city) && !city.isEmpty()) {
      filters.add(new FilterDefinition(FilterBy.CITY, city));
    }
    if (Objects.nonNull(departureEarliest)) {
      filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_EARLIEST, departureEarliest));
    }
    if (Objects.nonNull(departureLatest)) {
      filters.add(new FilterDefinition(FilterBy.DEPARTURE_TIME_LATEST, departureLatest));
    }
    if (Objects.nonNull(arrivalEarliest)) {
      filters.add(new FilterDefinition(FilterBy.ARRIVAL_TIME_EARLIEST, arrivalEarliest));
    }
    if (Objects.nonNull(arrivalLatest)) {
      filters.add(new FilterDefinition(FilterBy.ARRIVAL_TIME_LATEST, arrivalLatest));
    }
    return filters;
  }

  public FilterDefinitionBuilder flightType(FlightType flightType) {
    this.flightType = flightType;
    return this;
  }

  public FilterDefinitionBuilder departureCity(String departureCity) {
    this.departureCity = departureCity;
    return this;
  }

  public FilterDefinitionBuilder arrivalCity(String arrivalCity) {
    this.arrivalCity = arrivalCity;
    return this;
  }

  public FilterDefinitionBuilder city(String city) {
    this.city = city;
    return this;
  }

  public FilterDefinitionBuilder departureEarliest(Date departureEarliest) {
    this.departureEarliest = departureEarliest;
    return this;
  }

  public FilterDefinitionBuilder departureLatest(Date departureLatest) {
    this.departureLatest = departureLatest;
    return this;
  }

  public FilterDefinitionBuilder arrivalEarliest(Date arrivalEarliest) {
    this.arrivalEarliest = arrivalEarliest;
    return this;
  }

  public FilterDefinitionBuilder arrivalLatest(Date arrivalLatest) {
    this.arrivalLatest = arrivalLatest;
    return this;
  }
}
