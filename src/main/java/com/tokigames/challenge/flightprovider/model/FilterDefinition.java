package com.tokigames.challenge.flightprovider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterDefinition {

  private FilterBy filterBy;

  private Object filterValue;

}
