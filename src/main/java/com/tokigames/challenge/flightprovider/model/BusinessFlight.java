package com.tokigames.challenge.flightprovider.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tokigames.challenge.flightprovider.util.FlightDateDeserializer;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BusinessFlight {

  private String departure;

  private String arrival;

  @JsonDeserialize(converter = FlightDateDeserializer.class)
  private Date departureTime;

  @JsonDeserialize(converter = FlightDateDeserializer.class)
  private Date arrivalTime;

}
