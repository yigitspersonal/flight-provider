package com.tokigames.challenge.flightprovider.model;

public enum SortBy {
  NONE, TYPE, DEPARTURE_CITY, ARRIVAL_CITY, DEPARTURE_TIME, ARRIVAL_TIME
}
