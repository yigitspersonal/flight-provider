package com.tokigames.challenge.flightprovider.service;

import com.tokigames.challenge.flightprovider.client.BusinessFlightClient;
import com.tokigames.challenge.flightprovider.client.CheapFlightClient;
import com.tokigames.challenge.flightprovider.model.FilterDefinition;
import com.tokigames.challenge.flightprovider.model.Flight;
import com.tokigames.challenge.flightprovider.model.FlightType;
import com.tokigames.challenge.flightprovider.model.PageDefinition;
import com.tokigames.challenge.flightprovider.model.SortBy;
import com.tokigames.challenge.flightprovider.model.SortDefinition;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Slf4j
@Service
@RequiredArgsConstructor
public class FlightService {

  private final BusinessFlightClient businessFlightClient;

  private final CheapFlightClient cheapFlightClient;

  public Collection<Flight> getFlights(Collection<FilterDefinition> filters, SortDefinition sort,
      PageDefinition page) {

    Stream<Flight> flightStream = getAllFlightStream();
    flightStream = filterFlightsBy(flightStream, filters);
    flightStream = sortFlightsBy(flightStream, sort);
    flightStream = pageFlightsBy(flightStream, page);

    return flightStream.collect(Collectors.toList());
  }

  Collection<Flight> getAllFlights() {
    return getAllFlightStream().collect(Collectors.toList());
  }

  Stream<Flight> getAllFlightStream() {
    return Flux.concat(
        businessFlightClient.getAllFlightsAsync()
            .map(f -> Flight.builder()
                .departure(f.getDeparture())
                .arrival(f.getArrival())
                .departureTime(f.getDepartureTime())
                .arrivalTime(f.getArrivalTime())
                .type(FlightType.BUSINESS)
                .build()),
        cheapFlightClient.getAllFlightsAsync()
            .map(f -> Flight.builder()
                .departure(f.getDepartureCity())
                .arrival(f.getArrivalCity())
                .departureTime(f.getDeparture())
                .arrivalTime(f.getArrival())
                .type(FlightType.CHEAP)
                .build())
    ).toStream();
  }

  Stream<Flight> filterFlightsBy(Stream<Flight> flightStream,
      Collection<FilterDefinition> filters) {

    for (FilterDefinition filter : filters) {
      log.info("Filtering flights by {}={}", filter.getFilterBy(), filter.getFilterValue());
      switch (filter.getFilterBy()) {
        case TYPE:
          flightStream = flightStream.filter(flight ->
              Objects.equals(filter.getFilterValue(), flight.getType()));
          break;
        case DEPARTURE_CITY:
          flightStream = flightStream.filter(flight ->
              Objects.equals(filter.getFilterValue(), flight.getDeparture()));
          break;
        case ARRIVAL_CITY:
          flightStream = flightStream.filter(flight ->
              Objects.equals(filter.getFilterValue(), flight.getArrival()));
          break;
        case CITY:
          flightStream = flightStream.filter(flight ->
              Objects.equals(filter.getFilterValue(), flight.getDeparture())
                  || Objects.equals(filter.getFilterValue(), flight.getArrival()));
          break;
        case DEPARTURE_TIME_EARLIEST:
          flightStream = flightStream.filter(flight ->
              flight.getDepartureTime().after((Date) filter.getFilterValue()));
          break;
        case DEPARTURE_TIME_LATEST:
          flightStream = flightStream.filter(flight ->
              flight.getDepartureTime().before((Date) filter.getFilterValue()));
          break;
        case ARRIVAL_TIME_EARLIEST:
          flightStream = flightStream.filter(flight ->
              flight.getArrivalTime().after((Date) filter.getFilterValue()));
          break;
        case ARRIVAL_TIME_LATEST:
          flightStream = flightStream.filter(flight ->
              flight.getArrivalTime().before((Date) filter.getFilterValue()));
          break;
        default:
          throw new RuntimeException("Shouldn't have gotten here. Unrecognized filter type: "
              + filter.getFilterBy().toString());
      }
    }
    return flightStream;
  }

  Stream<Flight> sortFlightsBy(Stream<Flight> flightStream, SortDefinition sortDefinition) {
    if (SortBy.NONE.equals(sortDefinition.getSortBy())) {
      return flightStream;
    }
    return flightStream.sorted((f1, f2) -> {
      int comparison;
      switch (sortDefinition.getSortBy()) {
        case TYPE:
           comparison = f1.getType().compareTo(f2.getType());
           break;
        case DEPARTURE_CITY:
          comparison = f1.getDeparture().compareTo(f2.getDeparture());
          break;
        case ARRIVAL_CITY:
          comparison = f1.getArrival().compareTo(f2.getArrival());
          break;
        case DEPARTURE_TIME:
          comparison = f1.getDepartureTime().compareTo(f2.getDepartureTime());
          break;
        case ARRIVAL_TIME:
          comparison = f1.getArrivalTime().compareTo(f2.getArrivalTime());
          break;
        default:
          throw new RuntimeException("Shouldn't have gotten here. Unrecognized sort type: "
              + sortDefinition.getSortBy().toString());
      }
      return sortDefinition.getDescending() ? -comparison : comparison;
    });
  }

  Stream<Flight> pageFlightsBy(Stream<Flight> flightStream, PageDefinition pageDefinition) {
    return flightStream.skip(pageDefinition.getOffset()).limit(pageDefinition.getLimit());
  }

}

