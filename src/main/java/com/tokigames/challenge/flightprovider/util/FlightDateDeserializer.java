package com.tokigames.challenge.flightprovider.util;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class FlightDateDeserializer extends StdConverter<Double, Date> {

    @Override
    public Date convert(Double value) {
      return new Date(value.longValue() * 1000L);
    }
}
