package com.tokigames.challenge.flightprovider.client;

import com.tokigames.challenge.flightprovider.model.CheapFlight;
import com.tokigames.challenge.flightprovider.model.FlightListContainer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class CheapFlightClient {

  @Value("${flight-providers.cheap.url}")
  private String providerUrl;

  private final WebClient webClient;

  public Flux<CheapFlight> getAllFlightsAsync() {

    log.info("Retrieving Cheap type flights asynchronously");

    Mono<FlightListContainer<CheapFlight>> cheapFlightsContainerMono = webClient.get()
        .uri(providerUrl).accept(MediaType.APPLICATION_JSON).retrieve()
        .bodyToMono(new ParameterizedTypeReference<FlightListContainer<CheapFlight>>() {});

    return cheapFlightsContainerMono.flatMapIterable(FlightListContainer::getData);
  }
}
