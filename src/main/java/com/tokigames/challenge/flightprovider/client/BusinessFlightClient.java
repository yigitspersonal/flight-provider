package com.tokigames.challenge.flightprovider.client;

import com.tokigames.challenge.flightprovider.model.BusinessFlight;
import com.tokigames.challenge.flightprovider.model.FlightListContainer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class BusinessFlightClient {

  @Value("${flight-providers.business.url}")
  private String providerUrl;

  private final WebClient webClient;

  public Flux<BusinessFlight> getAllFlightsAsync() {

    log.info("Retrieving Business type flights asynchronously");

    Mono<FlightListContainer<BusinessFlight>> cheapFlightsContainerMono = webClient.get()
        .uri(providerUrl).accept(MediaType.APPLICATION_JSON).retrieve()
        .bodyToMono(new ParameterizedTypeReference<FlightListContainer<BusinessFlight>>() {});

    return cheapFlightsContainerMono.flatMapIterable(FlightListContainer::getData);
  }
}
