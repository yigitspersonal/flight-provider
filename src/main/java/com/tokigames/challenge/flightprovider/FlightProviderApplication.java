package com.tokigames.challenge.flightprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class FlightProviderApplication {

  public static void main(String[] args) {
    SpringApplication.run(FlightProviderApplication.class, args);
  }

  @Bean
  public WebClient getWebClient() {
    return WebClient.create();
  }

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
}
