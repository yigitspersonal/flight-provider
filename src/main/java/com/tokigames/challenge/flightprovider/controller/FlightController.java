package com.tokigames.challenge.flightprovider.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.tokigames.challenge.flightprovider.model.FilterDefinition;
import com.tokigames.challenge.flightprovider.model.FilterDefinitionBuilder;
import com.tokigames.challenge.flightprovider.model.Flight;
import com.tokigames.challenge.flightprovider.model.FlightType;
import com.tokigames.challenge.flightprovider.model.PageDefinition;
import com.tokigames.challenge.flightprovider.model.SortBy;
import com.tokigames.challenge.flightprovider.model.SortDefinition;
import com.tokigames.challenge.flightprovider.service.FlightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Validated
@RestController
@RequestMapping("/flights")
@RequiredArgsConstructor
@Api("Flight Controller - Get list of sorted, filtered flights")
public class FlightController {

  private final FlightService flightService;

  @ApiOperation("Get flights")
  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public Collection<Flight> getFlights(
      @RequestParam(value = "flight_type", required = false)
          @ApiParam(value = "Flight Type", allowableValues = "BUSINESS,CHEAP")
          FlightType flightType,

      @RequestParam(value = "departure", required = false)
          @ApiParam(value = "Departure City") String departureCity,

      @RequestParam(value = "arrival", required = false)
          @ApiParam(value = "Arrival City") String arrivalCity,

      @RequestParam(value = "city", required = false)
          @ApiParam(value = "Either Departure or Arrival City") String city,

      @RequestParam(value = "departure_after", required = false)
          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
          @ApiParam(value = "Departure After, in 'yyyy-MM-dd HH:mm' format") Date departureEarliest,

      @RequestParam(value = "departure_before", required = false)
          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
          @ApiParam(value = "Departure Before, in 'yyyy-MM-dd HH:mm' format") Date departureLatest,

      @RequestParam(value = "arrival_after", required = false)
          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
          @ApiParam(value = "Arrival After, in 'yyyy-MM-dd HH:mm' format") Date arrivalEarliest,

      @RequestParam(value = "arrival_before", required = false)
          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
          @ApiParam(value = "Arrival Before, in 'yyyy-MM-dd HH:mm' format") Date arrivalLatest,

      @RequestParam(value = "sort_by", defaultValue = "NONE")
          @ApiParam(value = "Sort by Field", allowableValues =
              "NONE,TYPE,DEPARTURE_CITY,ARRIVAL_CITY,DEPARTURE_TIME,ARRIVAL_TIME") SortBy sortBy,

      @RequestParam(value = "desc", defaultValue = "false")
          @ApiParam(value = "Whether to sort in descending order")
          Boolean descending,

      @RequestParam(value = "limit", defaultValue = "10")
          @ApiParam(value = "Pagination limit", allowableValues = "10,20,30,40,50") @Min(10) @Valid
          Integer limit,

      @RequestParam(value = "page", defaultValue = "1")
          @ApiParam(value = "Page number") @Min(1) @Valid Integer page) {

    Collection<FilterDefinition> filters = FilterDefinitionBuilder.builder().flightType(flightType)
        .arrivalCity(arrivalCity).departureCity(departureCity).city(city)
        .departureEarliest(departureEarliest).departureLatest(departureLatest)
        .arrivalEarliest(arrivalEarliest).arrivalLatest(arrivalLatest )
        .build();

    SortDefinition sortDefinition = new SortDefinition(sortBy, descending);
    PageDefinition pageDefinition = new PageDefinition(limit, page);

    // TODO - log filter, sorting and pagination details
    return flightService.getFlights(filters, sortDefinition, pageDefinition);
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handleConstraintViolationException(
      ConstraintViolationException ex) {

    log.info("Handling constraint violation in bad request");
    StringBuilder sb = new StringBuilder("Constraint violation in following fields ['");
    String separator = "";
    for (ConstraintViolation<?> v : ex.getConstraintViolations()) {
      sb.append(separator).append("FIELD: ").append(v.getPropertyPath()).append(", MESSAGE: ")
          .append(v.getMessage()).append(", INVALID VALUE: ").append(v.getInvalidValue());
      separator = "','";

      log.info("FIELD: {}, MESSAGE: {}, INVALID VALUE: {}", v.getPropertyPath(), v.getMessage(),
          v.getInvalidValue());
    }
    sb.append("']");

    return ResponseEntity.of(Optional.of(
        new ErrorResponse("Validation error", sb.toString(), HttpStatus.BAD_REQUEST)));
  }

  @ExceptionHandler
  public ResponseEntity<ErrorResponse> handleException(Exception ex, HttpServletRequest req) {
    log.info("Handling generic exception in HTTP {} for path: {}", req.getMethod(),
        req.getPathInfo());
    return ResponseEntity.of(Optional.of(
        new ErrorResponse(ex.getClass().getName(), ex.getMessage(),
            HttpStatus.INTERNAL_SERVER_ERROR)));
  }

  @Data
  @NoArgsConstructor
  static class ErrorResponse {

    private String error;

    private String description;

    private HttpStatus status;

    private int statusCode;

    ErrorResponse(String error, String description, HttpStatus status) {
      this.error = error;
      this.description = description;
      this.status = status;
      this.statusCode = status.value();
    }
  }
}
