# TokiGames Flight API [TEST]

## Introduction ##

This is a flights search API. It integrates multiple flight provider services into a single service,
where clients can use a single endpoint to retrieve, filter and sort aggregated flights. 

## Architecture, Libraries, Frameworks

This is a typical Spring Boot application, currently having a single REST endpoint to serve 
requests, and integrates 2 different flight providers. The choice of Java version for this service 
is Java 8.

##### Libraries & Frameworks

* Gradle
* Spring Boot Starter Web
* Lombok
* Swagger 2
* JUnit, Mockito

## Features

Provides a single endpoint @ `/api/v1/flights` with HTTP **GET** method, with following optional 
parameters:

##### Filter Parameters

* `flight_type`: One of `BUSINESS` or `CHEAP`
* `departure`: Filters by the departure city.
* `arrival`: Filters by the arrival city.
* `city`: Filters by either departure or arrival city.
* `departure_after`: Filters flights whose departure is after given value. Format: 
`yyyy-MM-dd HH:mm` 
after given time.
* `departure_before`: Filters flights whose departure is before given value. Format: 
`yyyy-MM-dd HH:mm` 
* `arrival_after`: Filters flights whose arrival is after given value. Format: `yyyy-MM-dd HH:mm` 
* `arrival_before`: Filters flights whose arrival is before given value. Format: `yyyy-MM-dd HH:mm` 

##### Sort Parameters

* `sort_by`: One of `NONE,TYPE,DEPARTURE_CITY,ARRIVAL_CITY,DEPARTURE_TIME,ARRIVAL_TIME`. Default 
sorting strategy is `NONE`
* `desc`: Whether to sort in descending order, default `false`.

##### Pagination

* `limit`: Number of elements returned in each page. Allowed values: `10,20,30,40,50`. Any value 
smaller than 10 returns an error response. Values larger than 50 are limited by 50, 
while values not divisible by 10 are automatically rounded down (e.g. limit=23 -> 20).
* `page`: Page number. First page number is `1`. Any non-positive page value returns an error 
response.

#### Swagger API location

Assuming default settings, there's a swagger documentation @ 
http://localhost:8080/api/v1/swagger-ui.html

## Development & Running

##### Prerequisites
Nothing is required really besides Java 8.

##### How to build & run
You can use the gradle wrapper to build and test the application:

$ ./gradlew clean build

Or without tests:
$ ./gradlew clean build -x test

To run the service locally:

$ ./gradlew bootRun

or

$ java -jar build/libs/flight-provider-1.0.0-SNAPSHOT.jar

##### Branching strategy

Standard git-flow branching strategy. Development branch is `develop`, Feature branches are 
`feature/<feature_name>`, bugfixes under `defect/<defect_name>`, refactorings `refactor/<target>`
etc.

##### Before committing / merging

* Make sure you covered your changes in unit tests.
* Make sure all tests pass and not @Ignore'd.
* Make sure you update this README if need be. 

## Future work

* Separate development/test/production profiles.
* Dockerize.
* Cursor-based pagination.

#### On async operations

Currently we're using reactive WebClient while making our calls to the flight providers. However,
fetched entities are blocked along the way, as we're converting them to streams and operating on 
them as such.

Future work would definitely include refactoring all service operations on the flux, and providing 
our `/flights` API also using reactive Flux.